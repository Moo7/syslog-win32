#!/usr/bin/env python
# -*- encoding: utf-8 -*-

from waflib.Tools.c import cstlib
cstlib.inst_to = '${PREFIX}/lib'

top = '.'
out = '.build'
prefix = 'out'

VERSION = '1.0.0'
APPNAME = 'syslog-win32'


def options(opt):
	opt.add_option('--prefix', dest='prefix', default=prefix, help='installation prefix [default: %r]' % prefix)
	opt.load('compiler_c')


def configure(conf):
	conf.load('compiler_c')


def build(bld):
	bld.shlib(
		target='syslog',
		vnum = VERSION,
		source=['src/syslog.c'],
		includes=['./include'],
		export_includes=['./include'],
		lib=['ws2_32', 'wsock32']
	)
	bld.program(
		target='logger', 
		source=['src/logger.c'],
		use=['syslog'],
		lib=['ws2_32', 'wsock32']
	)
	bld.install_files('${PREFIX}/include', ['include/syslog.h'])
	
