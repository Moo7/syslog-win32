Summary
-------
Syslog client for Windows.

Description
-----------
Provides syslog interface for MinGW applications.

Credits
-------
Clone from SourceForge "Syslog for Windows"; https://sourceforge.net/projects/syslog-win32/

